/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datagenerator.generator;

import datagenerator.datatype.AutoincrementType;
import datagenerator.datatype.ColumnDataType;
import datagenerator.datatype.DataTypeEnum;
import datagenerator.datatype.DateType;
import datagenerator.datatype.EmailType;
import datagenerator.datatype.NumberRangeType;
import datagenerator.datatype.TextType;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author filip
 */
public class DataGenerator implements IDataGenerator {

    
    private Sequencer[] sequencers;
    
    @Override
    public String[] getInsertSQL(ArrayList<ColumnDataType> columnDataType, int numberOfRows, String tableName) {

        sequencers = new Sequencer[columnDataType.size()];
        
        for (int i = 0; i <  columnDataType.size(); i++) {
            if(columnDataType.get(i).getDataTypeEnum() == DataTypeEnum.AUTOINCREMENT){
                AutoincrementType autoincrementType = (AutoincrementType) columnDataType.get(i);
                //System.out.println(autoincrementType.getStartAt() + " " +autoincrementType.getIncrement());
                sequencers[i] = new Sequencer(autoincrementType.getStartAt(), autoincrementType.getIncrement());
            }
        }
        
        String[] resultSql = new String[numberOfRows];

        for (int i = 0; i < numberOfRows; i++) {

            resultSql[i] = "insert into " + tableName + "(";

            for (int j = 0; j < columnDataType.size(); j++) {
                resultSql[i] += columnDataType.get(j).getName();
                if (j != columnDataType.size() - 1) {
                    resultSql[i] += ", ";
                }
            }

            resultSql[i] += ") values (";

            for (int j = 0; j < columnDataType.size(); j++) {
                resultSql[i] += getDataFromDataType(columnDataType.get(j), j);
                if (j != columnDataType.size() - 1) {
                    resultSql[i] += ", ";
                }
            }

            resultSql[i] += ");";
        }

        return resultSql;
    }

    @Override
    public String getEmailData() {
        return getOneWord() + "." + getOneWord() + "@" + "gmail.com";
    }

    @Override
    public String getTextData(int numberOfWords) {
        String randomStrings = "";
        Random random = new Random();
        for (int i = 0; i < numberOfWords; i++) {
            randomStrings += " " + getOneWord();
        }
        return randomStrings;
    }

    @Override
    public String getDateData(int MonthFrom, int MonthTo, int YearFrom, int YearTo) {
        int day = 1;
        int month = getNumberRangeData(MonthFrom, MonthTo);
        int year = getNumberRangeData(YearFrom, YearTo);

        String date = "0" + day;

        if (month < 10) {
            date += ".0" + month;
        } else {
            date += "." + month;
        }

        if (year < 10) {
            date += ".0" + year;
        } else {
            date += "." + year;
        }

        return date;
    }

    @Override
    public int getNumberRangeData(int from, int to) {
        Random random = new Random();
        return from + random.nextInt(to - from + 1);
    }

    @Override
    public int getAutoincrementData(Sequencer sequencer) {
        return sequencer.nextValue();
    }

    @Override
    public String getOneWord() {

        Random random = new Random();
        int wordLength = random.nextInt(8) + 3;
        String word = "";
        for (int j = 0; j < wordLength; j++) {
            word += (char) ('a' + random.nextInt(26));
        }
        return word;
    }

    @Override
    public String getDataFromDataType(ColumnDataType columnDataType, int columnNumber) {
        switch (columnDataType.getDataTypeEnum()) {
            case DATE:
                DateType dateType = (DateType) columnDataType;
                return getDateData(dateType.getMonthFrom(), dateType.getMonthTo(), dateType.getYearFrom(), dateType.getYearTo());
            case EMAIL:
                return getEmailData();
            case AUTOINCREMENT:
                AutoincrementType autoincrementType = (AutoincrementType) columnDataType;
                return String.valueOf(getAutoincrementData(sequencers[columnNumber]));
            case NUMBERRANGE:
                NumberRangeType numberRangeType = (NumberRangeType) columnDataType;
                return String.valueOf(getNumberRangeData(numberRangeType.getFrom(), numberRangeType.getTo()));
            case TEXT:
                TextType textType = (TextType) columnDataType;
                return getTextData(textType.getNumberOfWords());
            default:
                return "null";
        }
    }

}
