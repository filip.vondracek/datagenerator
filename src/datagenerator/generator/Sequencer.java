/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datagenerator.generator;

/**
 *
 * @author filip
 */
public class Sequencer {
    
    private int startAt;
    private int increment;
    private int currentValue;

    public Sequencer(int startAt, int increment) {
        this.startAt = startAt;
        this.increment = increment;
        this.currentValue = startAt;
    }
    
    public int nextValue(){
        return currentValue += increment;
    }
    
    public int getStartAt() {
        return startAt;
    }

    public void setStartAt(int startAt) {
        this.startAt = startAt;
    }

    public int getIncrement() {
        return increment;
    }

    public void setIncrement(int increment) {
        this.increment = increment;
    }

    public int getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(int currentValue) {
        this.currentValue = currentValue;
    }
    
    
    
}
