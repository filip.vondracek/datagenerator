/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datagenerator.generator;

import datagenerator.datatype.ColumnDataType;
import java.util.ArrayList;

/**
 *
 * @author filip
 */
public interface IDataGenerator {
    
    /**
     *
     * @param columnDataType
     * @return 
     */
    String[] getInsertSQL(ArrayList<ColumnDataType> columnDataType, int numberOfRows, String tableName);
    
    String getDataFromDataType(ColumnDataType columnDataType, int columnNumber);
    
    String getEmailData();
       
    String getTextData(int numberOfWords);
    
    String getDateData(int MonthFrom, int MonthTo, int YearFrom, int YearTo);
    
    int getNumberRangeData(int from, int to);
    
    int getAutoincrementData(Sequencer sequencer);
 
    String getOneWord();
}
