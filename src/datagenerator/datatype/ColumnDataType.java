/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datagenerator.datatype;

/**
 *
 * @author filip
 */
public class ColumnDataType {
    
    private String name;
    private int dataType;
    private DataTypeEnum dataTypeEnum;

    public ColumnDataType(String name, int dataType, DataTypeEnum dataTypeEnum) {
        this.name = name;
        this.dataType = dataType;
        this.dataTypeEnum = dataTypeEnum;
    }

    public ColumnDataType() {
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDataType() {
        return dataType;
    }

    public void setDataType(int dataType) {
        this.dataType = dataType;
    }

    public DataTypeEnum getDataTypeEnum() {
        return dataTypeEnum;
    }

    public void setDataTypeEnum(DataTypeEnum dataTypeEnum) {
        this.dataTypeEnum = dataTypeEnum;
    }
    
    
    
}

