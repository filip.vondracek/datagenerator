/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datagenerator.datatype;

/**
 *
 * @author filip
 */
public class AutoincrementType extends ColumnDataType{
    
    private int startAt;
    
    private int increment;

    public AutoincrementType(int startAt, int increment, String name, int dataType) {
        super(name, dataType, DataTypeEnum.AUTOINCREMENT);
        this.startAt = startAt;
        this.increment = increment;
    }

    public int getStartAt() {
        return startAt;
    }

    public void setStartAt(int startAt) {
        this.startAt = startAt;
    }

    public int getIncrement() {
        return increment;
    }

    public void setIncrement(int increment) {
        this.increment = increment;
    }
    
    
}
