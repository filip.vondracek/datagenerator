/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datagenerator.datatype;

/**
 *
 * @author filip
 */
public class DateType extends ColumnDataType{
    
    private int MonthFrom;
    private int MonthTo;
    private int YearFrom;
    private int YearTo;

    public DateType(int MonthFrom, int MonthTo, int YearFrom, int YearTo, String name, int dataType) {
        super(name, dataType, DataTypeEnum.DATE);
        this.MonthFrom = MonthFrom;
        this.MonthTo = MonthTo;
        this.YearFrom = YearFrom;
        this.YearTo = YearTo;
    }

    public int getMonthFrom() {
        return MonthFrom;
    }

    public void setMonthFrom(int MonthFrom) {
        this.MonthFrom = MonthFrom;
    }

    public int getMonthTo() {
        return MonthTo;
    }

    public void setMonthTo(int MonthTo) {
        this.MonthTo = MonthTo;
    }

    public int getYearFrom() {
        return YearFrom;
    }

    public void setYearFrom(int YearFrom) {
        this.YearFrom = YearFrom;
    }

    public int getYearTo() {
        return YearTo;
    }

    public void setYearTo(int YearTo) {
        this.YearTo = YearTo;
    }
    
    
    
}
