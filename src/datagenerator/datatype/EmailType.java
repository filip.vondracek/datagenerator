/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datagenerator.datatype;

/**
 *
 * @author filip
 */
public class EmailType extends ColumnDataType{

    public EmailType(String name, int dataType) {
        super(name, dataType, DataTypeEnum.EMAIL);
    }
        
}
