/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datagenerator.datatype;

/**
 *
 * @author filip
 */
public class TextType extends ColumnDataType{
    
    private int numberOfWords;

    public int getNumberOfWords() {
        return numberOfWords;
    }

    public TextType(int numberOfWords, String name, int dataType) {
        super(name, dataType, DataTypeEnum.TEXT);
        this.numberOfWords = numberOfWords;
    }
     
    public void setNumberOfWords(int numberOfWords) {
        this.numberOfWords = numberOfWords;
    }
    
    
    
}
